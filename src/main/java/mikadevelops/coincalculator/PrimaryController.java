package mikadevelops.coincalculator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class PrimaryController {
	
	private File pickedFile;
	private Mat sourceGray = new Mat();
	private int threshold = 100;
	
	@FXML
    private Button buttonChooseFile;
	
	@FXML
	private ImageView imageView;
	
    @FXML
    private ImageView imageViewGray;

	@FXML
	private Label labelFileName;

	@FXML
	private Label labelNumberOfCoins;
	
    @FXML
    private Label labelMonetaryValue;

	/**
	 * Action for chosen file and shows image on window and finds contours.
	 * OpenCV examples used: <a href="https://docs.opencv.org/3.4/df/d0d/tutorial_find_contours.html">opencv.org find contours</a>, 
	 * <a href="https://docs.opencv.org/3.4/d4/d70/tutorial_hough_circle.html">opencv.org hough circles</a>
	 * and <a href="https://www.geeksforgeeks.org/how-to-detect-shapes-in-images-in-python-using-opencv/">geeksforgeeks.org</a>
	 * 
	 * @param event
	 */
	@FXML
    void actionChooseFile(ActionEvent event) {
		FileChooser pickAfile = new FileChooser();
		pickedFile = pickAfile.showOpenDialog(new Stage());
		
		// if file is chosen, show image
		if (pickedFile != null) {
			
			showImage(pickedFile);
			
			ArrayList<Integer> coinArray = new ArrayList<Integer>();
			
			Mat sourceImage = Imgcodecs.imread(pickedFile.getPath());
			Imgproc.cvtColor(sourceImage, sourceGray, Imgproc.COLOR_BGR2GRAY);
			// I have no idea why this is used. Blur is better? Have to see about it.
			Imgproc.medianBlur(sourceGray, sourceGray, 5);
			
			Mat circles = new Mat();
			Imgproc.HoughCircles(sourceGray, circles, Imgproc.HOUGH_GRADIENT,
					1.0, (double)sourceGray.rows()/50,100.0,25.0,18,60);
			
			for (int i = 0; i < circles.cols(); i++) {
				double[] circleArray = circles.get(0, i);
				Point center = new Point(Math.round(circleArray[0]),
						Math.round(circleArray[1]));
				
				// Draw centres for found circles
				Imgproc.circle(sourceGray, center, 2, new Scalar(0,0,0),
						3,8,0);
				
				// add radiuses to coin array
				coinArray.add((int)Math.round(circleArray[2]));
				
				int radius = (int)Math.round(circleArray[2]);
				
				// Draw found circle outlines
				Imgproc.circle(sourceGray, center, radius, new Scalar(255,255,255),
						3,8,0);
				
			}
			
			// Show gray scale image
			MatOfByte byteMat = new MatOfByte();
			Imgcodecs.imencode(".bmp", sourceGray, byteMat);
			Image grayImage = new Image(new ByteArrayInputStream(byteMat.toArray()));
			imageViewGray.setImage(grayImage);
			
			// Show number of detected coins
			labelNumberOfCoins.setText(Integer.toString(coinArray.size()));
			
			// Sort array
			coinArray.sort(Comparator.naturalOrder());
			
			// Count monetary value and display result
			countMoney(coinArray);
		}
    }
	
	/**
	 * Shows chosen image.
	 * @param imageFile File to load to window.
	 */
	private void showImage(File imageFile) {
		
		try {
			InputStream stream = new FileInputStream(imageFile.getPath());
			labelFileName.setText(imageFile.getName());
			Image image = new Image(stream);
			imageView.setImage(image);
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
	}
	
	/**
	 * Counts monetary value. Assumes that coins are 1, 2, 5, 10 eurocent
	 * in value and uses radius to sort. Measures taken from table.png.
	 * 
	 * @param coins ArrayList of coin radiuses
	 */
	private void countMoney(ArrayList coins) {
		
		int monetaryValue = 0;
		
		for (int i=0; i<coins.size(); i++) {
			
			int radius = (int)coins.get(i);
			
			if ( radius >= 21 &&  radius < 34 ) {
				monetaryValue += 2;
			}else if( radius >= 34 &&  radius < 46 ) {
				monetaryValue += 10;
			}else if( radius >= 46 &&  radius < 60 ) {
				monetaryValue += 5;
			}
		}
		
		Double euros = (double)monetaryValue/100;
		
		labelMonetaryValue.setText(Double.toString(euros));
		
	}
}