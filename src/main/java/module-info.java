module mikadevelops.coincalculator {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.base;
	requires javafx.graphics;
	requires opencv;

    opens mikadevelops.coincalculator to javafx.fxml;
    exports mikadevelops.coincalculator;
}
